#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Sapit (Spotify API Tester)

A simple class to test various Spotify API endpoints and authentication
methods. This class uses the standard Python library `Cmd` command line
framework to create a simple command line interpreter encapsulating API
calls as simple action methods.

See the README file for implementation details.

v1.0.0 - Initial release

"""

__author__ = "pparise"
__version__ = "1.0.0"
__license__ = "MIT"

# standard library modules
import sys
import os
import datetime
import textwrap
import html
import csv
import json
import re
from cmd import Cmd

# 3rd party modules
import spotipy
import spotipy.util as util
from spotipy.oauth2 import SpotifyClientCredentials
from tabulate import tabulate

# local modules
import secrets


class Sapit(Cmd):

    def __init__(self):
        Cmd.__init__(self)
        self.prompt = ' 🎵 '
        self.intro = '\n\033[32m Welcome to SAPIT, the Spotify API Tester! Type help or ? to list commands.\033[0m\n'
        self.nohelp = self._indent('Hummm 🤔 no help on `%s`. Are you sure this is the right command?\n')

    def __enter__(self):
        return self

    # command methods

    def do_album(self, album_id):
        """Retrieve album track information."""

        # validate that an album ID was passed in
        if album_id == '':
            msg = self._indent('Oops ☹️  missing album id parameter! Type `help album` for more information.')
            print(msg, '\n')
            return

        # get the album info
        album_info = self.get_album_info(album_id)

        # if there was no spotify API error
        if album_info is not None:

            # build and print the info header
            album_header = '\n\033[1m\033[93mAlbum Information\033[0m\n'
            album_header += '-' * 65 + '\n'
            album_header += 'ID       : ' + album_id + '\n'
            album_header += 'Name     : ' + album_info['name'] + '\n'
            album_header += 'Released : ' + album_info['release_date'] + '\n'
            album_header += 'Artist   : ' + album_info['artists'][0]['name'] + '\n'
            if 'genres' in album_info:
                genres = ','.join(album_info['genres'])
            else:
                genres = 'NA'
            album_header += 'Genres   : ' + genres + '\n'
            print(self._indent(album_header))

            # pretty format the response as tabular data and print it
            table = []
            headers = ['#', 'Title', 'Duration']
            total = 0
            for track in album_info['tracks']['items']:
                number = track['track_number']
                name = track['name']
                duration = track['duration_ms']
                total += duration
                duration = self.convert_duration(duration)
                data = [number, name, duration]
                table.append(data)
            total_row = [None, None, self.convert_duration(total)]
            table.append(total_row)
            table = tabulate(table, headers=headers)
            print(self._indent(table), '\n')

    def do_artist(self, artist_id):
        """Retrieve artist information (albums and compilations only)."""

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # validate that an artist ID was passed in
        if artist_id == '':
            msg = self._indent('Oops ☹️  missing artist id parameter! Type `help artist` for more information.')
            print(msg, '\n')
            return

        # get the basic artist info
        artist_info = self.get_artist_info(artist_id)

        # if there was no spotify API error
        if artist_info is not None:

            # build and print the info header
            artist_header = '\n\033[1m\033[93m' + artist_info['name'] + ' Discography\033[0m\n'
            artist_header += '-' * 65 + '\n'
            artist_header += 'ID     : ' + artist_id + '\n'
            artist_header += 'Name   : ' + artist_info['name'] + '\n'
            if 'genres' in artist_info:
                genres = ','.join(artist_info['genres'])
            else:
                genres = 'NA'
            artist_header += 'Genres : ' + genres + '\n'
            print(self._indent(artist_header))

            # setup the query parameters and make the request
            artist_id = 'spotify:artist:' + artist_id
            offset = 0
            include = 'album,compilation'
            country = 'CA'
            try:
                response = sp.artist_albums(artist_id, country=country, offset=offset, album_type=include)
            except spotipy.client.SpotifyException as err:
                message = err.msg.replace('\n', ' ')
                print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
                print(self._indent(message), '\n')
                return

            # pretty format the response as tabular data and print it
            table = []
            headers = ['Album ID', 'Album Name', 'Album Type', 'Release', 'Tracks']
            for album in response['items']:
                id = album['id']
                name = album['name']
                name = (name[:50] + '..') if len(name) > 50 else name
                album_type = album['album_type']
                release_date = album['release_date']
                tracks = album['total_tracks']
                data = [id, name, album_type, release_date, tracks]
                table.append(data)
            table = tabulate(table, headers=headers)
            print(self._indent(table), '\n')

            # setup the file name and save the data to a file
            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
            artist_header = ansi_escape.sub('', artist_header)
            artist_name = artist_info['name'].lower().replace(' ', '-')
            filename = 'artist-' + artist_name + '-' + self.get_date() + '.txt'
            self.save_data(table, artist_header, filename)

    def do_favorites(self, arg):
        """Retrieve a user's saved (favorite) tracks."""

        # retrieve or refresh a scoped user token
        username = secrets.spotify_user_id
        scope = 'user-library-read'
        token = util.prompt_for_user_token(
            username,
            scope,
            client_id=secrets.client_id,
            client_secret=secrets.client_secret,
            redirect_uri=secrets.redirect_uri
        )

        # we have a token!
        if token:

            # instantiate an authenticated Spotify API client
            sp = spotipy.Spotify(auth=token)

            # make the request
            try:
                results = sp.current_user_saved_tracks()
            except spotipy.client.SpotifyException as err:
                message = err.msg.replace('\n', ' ')
                print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
                print(self._indent(message), '\n')
                return

            # build and print the info header
            header = self._indent('\033[1m\033[93m' + username.upper() + '\'s Favorite Tracks (Track Name - Artist)\033[0m')
            print('\n', header, '\n', sep='')

            # print the response
            for item in results['items']:
                track = item['track']
                print(self._indent(track['name'] + ' - ' + track['artists'][0]['name']))
        else:
            print(self._indent('Oops  ☹️  can\'t get token for', username))

        print('')

    def do_playlists(self, arg):
        """Retrieve a user's public playlists."""

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # setup the query parameters and make the request
        user = secrets.spotify_user_id
        try:
            response = sp.user_playlists(user, limit=50, offset=0)
        except spotipy.client.SpotifyException as err:
            message = err.msg.replace('\n', ' ')
            print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
            print(self._indent(message), '\n')
            return

        # build and print the info header
        header = self._indent('\033[1m\033[93m' + user.upper() + '\'s Public Playlists\033[0m')
        print('\n', header, '\n', sep='')

        # pretty format the response as tabular data and print it
        table = []
        headers = ['Playlist ID', 'Playlist Name', 'Playlist Description']
        for playlist in response['items']:
            id = playlist['id']
            name = playlist['name']
            description = html.unescape(playlist['description'])
            description = (description[:78] + '..') if len(description) > 78 else description
            tracks = playlist['tracks']['total']
            data = [id, name, description]
            table.append(data)
        table = tabulate(table, headers=headers)
        print(self._indent(table), '\n')

        # setup the file name and save the data to a file
        filename = user + '-playlists-' + self.get_date() + '.txt'
        self.save_data(table, None, filename)

    def do_quit(self, arg):
        """Exit the application. Shorthand: Ctrl-D."""
        msg = self._indent('Bye Bye 🎶 🎶 🎶')
        print('\n', msg, '\n', sep='')
        self.exit = True
        return True

    def do_search(self, arg):
        """Search for an artist."""

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # validate that a search argument was passed in
        if arg == '':
            print(self._indent('Oops ☹️  missing search criteria! Type `help search` for more information.'), '\n')
            return

        # setup the query parameters and make the request (keywords are matched in any order)
        q = arg.replace(' ', '+')
        t = 'artist'
        market = 'CA'
        try:
            response = sp.search(q, limit=20, offset=0, type=t, market=market)
        except spotipy.client.SpotifyException as err:
            message = err.msg.replace('\n', ' ')
            print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
            print(self._indent(message), '\n')
            return

        # build and print the info header
        header = self._indent('\033[1m\033[93mSpotify search results for `' + arg + '`\033[0m')
        print('\n', header, '\n', sep='')

        # pretty format the response as tabular data and print it
        table = []
        headers = ['Artist ID', 'Artist Name', 'Genres']
        for artist in response['artists']['items']:
            id = artist['id']
            name = artist['name']
            name = (name[:40] + '..') if len(name) > 40 else name
            if 'genres' in artist:
                genres = ','.join(artist['genres'])
            else:
                genres = 'NA'
            genres = (genres[:60] + '..') if len(genres) > 60 else genres
            data = [id, name, genres]
            table.append(data)
        print(self._indent(tabulate(table, headers=headers)), '\n')

    def do_tracks(self, playlist_id):
        """Retrieve track information for a given playlist."""

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # validate that a playlist ID was passed in
        if playlist_id == '':
            msg = self._indent('Oops ☹️  missing playlist id parameter! Type `help tracks` for more information.')
            print(msg, '\n')
            return

        # get the basic playlist info
        playlist_info = self.get_playlist_info(playlist_id)

        # if there was no spotify API error
        if playlist_info is not None:

            # build and print the info header
            playlist_header = '\n\033[1m\033[93m' + secrets.spotify_user_id.upper() + ' Spotify Playlist\033[0m\n'
            playlist_header += '-' * 65 + '\n'
            playlist_header += 'ID     : ' + playlist_id + '\n'
            playlist_header += 'Name   : ' + playlist_info['name'] + '\n'
            playlist_header += 'Descr  : ' + html.unescape(playlist_info['description']) + '\n'
            playlist_header += 'Tracks : ' + str(playlist_info['tracks']['total']) + '\n'
            print(self._indent(playlist_header))

            # setup the query parameters and make the request
            playlist_id = 'spotify:playlist:' + playlist_id
            offset = 0
            fields = 'items(added_by.id,track(name,href,album(name,href),artists(name,href),duration_ms))'
            try:
                response = sp.playlist_tracks(playlist_id, offset=offset, fields=fields)
            except spotipy.client.SpotifyException as err:
                message = err.msg.replace('\n', ' ')
                print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
                print(self._indent(message), '\n')
                return

            # pretty format the response as tabular data and print it
            table = []
            headers = ['Track', 'Artist', 'Album', 'Duration']
            total = 0
            for tracks in response['items']:
                name = tracks['track']['name']
                artist = tracks['track']['artists'][0]['name']
                album = tracks['track']['album']['name']
                duration = tracks['track']['duration_ms']
                total += duration
                # for the console screen display (based on a width of 132)
                name = (name[:36] + '..') if len(name) > 36 else name
                artist = (artist[:36] + '..') if len(artist) > 36 else artist
                album = (album[:36] + '..') if len(album) > 36 else album
                duration = self.convert_duration(duration)
                data = [name, artist, album, duration]
                table.append(data)
            total_row = [None, None, None, self.convert_duration(total)]
            table.append(total_row)
            table = tabulate(table, headers=headers)
            print(self._indent(table), '\n')

            # setup the file name and save the data to a file
            ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
            playlist_header = ansi_escape.sub('', playlist_header)
            user = secrets.spotify_user_id
            playlist_name = playlist_info['name'].lower().replace(' ', '-')
            playlist_id = playlist_info['id']
            filename = user + '-' + playlist_name + '-' + self.get_date() + '.txt'
            self.save_data(table, playlist_header, filename)

    # non-command methods

    def get_album_info(self, album_id):
        """Retrieve basic info on an album.
        This method uses Client Credentials Flow for authentication.
        Endpoint: https://api.spotify.com/v1/albums/{id}
        """

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # setup the query parameters and make the request
        artist_id = 'spotify:album:' + album_id
        try:
            response = sp.album(album_id)
        except spotipy.client.SpotifyException as err:
            message = err.msg.replace('\n', ' ')
            print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
            print(self._indent(message), '\n')
            return None

        # return the request response
        return response

    def get_artist_info(self, artist_id):
        """Retrieve basic info on an artist.
        This method uses Client Credentials Flow for authentication.
        Endpoint: https://api.spotify.com/v1/artists/{id}
        """

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # setup the query parameters and make the request
        artist_id = 'spotify:artist:' + artist_id
        try:
            response = sp.artist(artist_id)
        except spotipy.client.SpotifyException as err:
            message = err.msg.replace('\n', ' ')
            print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
            print(self._indent(message), '\n')
            return None

        # return the request response
        return response

    def get_playlist_info(self, playlist_id):
        """Retrieve basic info on a playlist.
        This method uses Client Credentials Flow for authentication.
        Endpoint: https://api.spotify.com/v1/playlists/{playlist_id}
        Note: Returns info on public playlists only.
        """

        # instantiate an authenticated Spotify API client
        sp = spotipy.Spotify(
            client_credentials_manager=SpotifyClientCredentials(
                client_id=secrets.client_id,
                client_secret=secrets.client_secret
            )
        )

        # setup the query parameters and make the request
        playlist_id = 'spotify:playlist:' + playlist_id
        market = 'CA'
        fields = 'id,description,name,owner(display_name),tracks.total'
        try:
            response = sp.playlist(playlist_id, market=market, fields=fields)
        except spotipy.client.SpotifyException as err:
            message = err.msg.replace('\n', ' ')
            print(self._indent('Dooh! 🔥🔥🔥'), err.http_status, '🔥🔥🔥')
            print(self._indent(message), '\n')
            return None

        # return the request response
        return response

    def save_data(self, data, header, filename):
        """Save formatted data to a file."""
        fullpath = os.path.join(os.path.dirname(__file__), 'data/' + filename)
        with open(fullpath, mode='w', encoding='UTF-8') as f:
            if header is not None:
                f.write(header)
                f.write('\n')
            f.write(data)
            f.write('\n')
            f.close()

    def save_json(self, data, filename):
        """Save JSON data to a file."""
        fullpath = os.path.join(os.path.dirname(__file__), 'data/' + filename)
        with open(fullpath, mode='w', encoding='UTF-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

    # command help topics

    def help_album(self):
        doc = """
        Purpose  : Retrieve album track information.
        Param    : An album ID.
        Auth     : This method uses Client Credentials Flow for authentication.
        Endpoint : https://api.spotify.com/v1/albums/{id}
        """
        print(doc)

    def help_artist(self):
        doc = """
        Purpose  : Retrieve artist information (albums and compilations only).
        Param    : An artist ID.
        Auth     : This method uses Client Credentials Flow for authentication.
        Endpoint : https://api.spotify.com/v1/artists/{id}
                   https://api.spotify.com/v1/artists/{id}/albums
        Notes    : Columns may be truncated to fit the screen.
                   On Spotify an album can have more than one catalogue number.
        """
        print(doc)

    def help_favorites(self):
        doc = """
        Purpose  : Retrieve a user's saved (favorite) tracks.
        Param    : Uses the spotify user id from the secrets file.
        Auth     : This method uses Authorization Code Flow for authentication.
        Endpoint : https://api.spotify.com/v1/me/tracks
        Notes    : A valid access token with the proper scope must be acquired.
                   Follow instructions when prompted.
                   The token will be cached locally until it expires.
        """
        print(doc)

    def help_help(self):
        doc = """
        List available commands with `help` or detailed help with `help <cmd>`.
        """
        print(doc)

    def help_playlists(self):
        doc = """
        Purpose  : Retrieve a user's public playlists.
        Param    : Uses the spotify user id from the secrets file.
        Auth     : This method uses Client Credentials Flow for authentication.
        Endpoint : https://api.spotify.com/v1/users/{user_id}/playlists
        Notes    : Columns may be truncated to fit the screen.
        """
        print(doc)

    def help_quit(self):
        doc = """
        Exit the application. Shorthand: Ctrl-D.
        """
        print(doc)

    def help_search(self):
        doc = """
        Purpose  : Search for an artist.
        Param    : A search string. Can be multiple words.
        Auth     : This method uses Client Credentials Flow for authentication.
        Endpoint : https://api.spotify.com/v1/search
        Notes    : Keywords are matched in any order.
                   Columns may be truncated to fit the screen.
        """
        print(doc)

    def help_tracks(self):
        doc = """
        Purpose  : Retrieve track information for a given playlist.
        Param    : A playlist ID.
        Auth     : This method uses Client Credentials Flow for authentication.
        Endpoint : https://api.spotify.com/v1/playlists/{playlist_id}
                   https://api.spotify.com/v1/playlists/{playlist_id}/tracks
        Notes    : Columns may be truncated to fit the screen.
        """
        print(doc)

    # Cmd method overrides

    def cmdloop(self, intro=None):
        """Method override to handle a KeyboardInterrupt (aka CTRL-C)."""
        print(self.intro)
        while True:
            try:
                super(Sapit, self).cmdloop(intro='')
                break
            except KeyboardInterrupt:
                # print('^C detected. To quit type `quit` of CTRL-C.')
                pass

    def default(self, arg):
        """Method override to handle unknown input."""
        self.stdout.write('%s\n' % self._indent('Hummm 🤔 Unknown syntax or command: %s\n' % arg))

    def emptyline(self):
        """If an empty line was entered at the command line, do nothing."""
        pass

    def print_topics(self, header, cmds, cmdlen, maxcol):
        """Method override to handle help topics formatting."""
        if cmds:
            self.stdout.write('%s\n' % self._indent(str(header)))
            if self.ruler:
                self.stdout.write('%s\n' % self._indent(str(self.ruler * len(header))))
            cmds.remove('EOF')
            self.stdout.write('%s\n' % self._indent('  '.join(cmds)))
            # for cmd in cmds:
            #    self.stdout.write(self._indent(cmd))
            # self.columnize(cmds, maxcol - 1)
            self.stdout.write('\n')

    # static helper methods

    @staticmethod
    def get_date():
        """Return the current date in ISO-8601 format."""
        d = datetime.datetime.now()
        return d.strftime('%Y-%m-%d')

    @staticmethod
    def convert_duration(ms):
        """Convert milliseconds to a minutes and seconds format."""
        seconds = (ms / 1000) % 60
        seconds = int(seconds)
        minutes = (ms / (1000 * 60)) % 60
        minutes = int(minutes)
        hours = (ms / (1000 * 60 * 60)) % 24
        hours = int(hours)
        return datetime.time(hours, minutes, seconds).strftime("%H:%M:%S")

    @staticmethod
    def _indent(str):
        """Indent a string with spaces."""
        prefix = ' ' * 4
        return textwrap.indent(str, prefix)

    # redirect EOF handlers to quit handlers
    do_EOF = do_quit
    help_EOF = help_quit

if __name__ == '__main__':
    Sapit().cmdloop()
