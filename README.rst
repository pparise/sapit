Sapit (Spotify API Tester)
==========================

.. image:: sapit-logo-blue.png
   :width: 160
   :alt: Sapit


**Sapit** is a command line tool to test various Spotify API endpoints and
authentication methods. This class uses the standard Python library ``Cmd``
command line framework to create an interactive command line interface.

Much of the authentication complexity is abstracted by the ``spotipy`` library.

Private information is stored in a ``secrets.py`` file and imported at run time.
Create it and add your personal information as in the following example:

.. code-block:: python

    spotify_user_id = 'your_spotify_username'
    client_id = 'your_app_client_id'
    client_secret = 'your_app_client_secret'
    redirect_uri = 'your_app_redirect_uri'

This file is **not** included in the repository. Make sure to add the file to your .gitignore!



* Free software: MIT license
* Documentation: This file and inline.


Important Note
--------------
Given that artist, song, or album information can be of varying length, and given that everyone
has their own console preferences, printing to the console can give some vastly different and weird
looking results because of line wrapping. For these reasons some fields are truncated by design to
fit in most cases on a console with 132 columns. Your results may vary.


Features
--------
* Retrieve a user's public playlists.
* Retrieves track information for a given playlist.
* Retrieve a user's saved (favorite) tracks.
* Search for an artist.
* Retrieve an artists album discography.
* Retrieve album tracks.
* Save playlists and track lists to text files.



Credits
-------

Go to the `Spotify for Developers`_ website to register an app to get a client id,
a client secret, and specify a redirect uri.

Visit the `developer console`_ to check out and try the available endpoints.

Look at the `API docs`_ for more detailed information including detailed response information.

Documentation for the `Spotipy`_ library.

Documentation for the Python `Cmd`_ class.


.. _`Spotify for Developers`: https://developer.spotify.com

.. _`developer console`: https://developer.spotify.com/console/

.. _`API docs`: https://developer.spotify.com/documentation/web-api/

.. _`Spotipy`: https://spotipy.readthedocs.io/en/2.9.0/

.. _`Cmd`: https://docs.python.org/3/library/cmd.html
